#!/usr/bin/python
import math
class HinhVuong:
    """Dinh nghia class la hinh vuong"""
	def __init__(self, canh):
		self.canh = canh

	def chuvi(self):
		p = 4*self.canh			# Chu vi cua hinh vuong
		return p

	def dientich(self):
		s = math.pow(self.canh, 2)	# Dien tich cua hinh vuong
		return s

for i in range (10):
	hv = HinhVuong(i)			# Hinh vuong co canh la i
	print "Dien tich hinh vuong la: {}".format(hv.dientich())
