# Sử dụng tiếng Việt trong gói lệnh listings

Thực hiện: Thi Minh Nhựt - Email: thiminhnhut@gmail.com

Thời gian: Ngày 7 tháng 01 năm 2017

## Hướng dẫn cơ bản:

* File PDF: [tiengviet-trong-listings.pdf](https://gitlab.com/thiminhnhut/latex/blob/master/tutorials/filetex-samples/ex-listings/tiengviet-listings/tiengviet-trong-listings.pdf)

* File TeX: [tiengviet-trong-listings.tex](https://gitlab.com/thiminhnhut/latex/blob/master/tutorials/filetex-samples/ex-listings/tiengviet-listings/tiengviet-trong-listings.tex)

* Các file TeX ví dụ: [examples](https://gitlab.com/thiminhnhut/latex/tree/master/tutorials/filetex-samples/ex-listings/tiengviet-listings/examples).
