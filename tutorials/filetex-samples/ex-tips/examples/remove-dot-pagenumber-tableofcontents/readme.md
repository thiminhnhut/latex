# Xóa dấu chấm và số trang trong bảng mục lục

Thực hiện: Thi Minh Nhựt - Email: thiminhnhut@gmail.com

Thời gian: Ngày 11 tháng 01 năm 2017

## Hướng dẫn cơ bản:

* File PDF: [remove-dot-pagenumber-tableofcontents.pdf](https://gitlab.com/thiminhnhut/latex/blob/master/tutorials/filetex-samples/ex-tips/examples/remove-dot-pagenumber-tableofcontents/remove-dot-pagenumber-tableofcontents.pdf)

* File TeX: [remove-dot-pagenumber-tableofcontents.tex](https://gitlab.com/thiminhnhut/latex/blob/master/tutorials/filetex-samples/ex-tips/examples/remove-dot-pagenumber-tableofcontents/remove-dot-pagenumber-tableofcontents.tex)

* Các file TeX ví dụ: [examples](https://gitlab.com/thiminhnhut/latex/blob/master/tutorials/filetex-samples/ex-tips/examples/remove-dot-pagenumber-tableofcontents/examples).
