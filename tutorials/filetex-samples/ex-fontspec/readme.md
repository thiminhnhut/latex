# Sử dụng font Times New Roman trong LaTeX với XeLaTeX hoặc LuaLaTeX

Thực hiện: Thi Minh Nhựt - Email: thiminhnhut@gmail.com

Thời gian: Ngày 05 tháng 01 năm 2017

## Hướng dẫn cơ bản:

* File PDF: [sudungfont_timesnewroman.pdf](https://gitlab.com/thiminhnhut/latex/tree/master/tutorials/filetex-samples/ex-fontspec/sudungfont_timesnewroman.pdf)

* File TeX: [sudungfont_timesnewroman.tex](https://gitlab.com/thiminhnhut/latex/tree/master/tutorials/filetex-samples/ex-fontspec/sudungfont_timesnewroman.tex)
