# Chèn bảng từ file csv vào tài liệu LaTeX

Thực hiện: Thi Minh Nhựt - Email: thiminhnhut@gmail.com

Thời gian: Ngày 8 tháng 01 năm 2017

## Hướng dẫn cơ bản:

* File PDF: [chenbang_filecsv_latex.pdf](https://gitlab.com/thiminhnhut/latex/tree/master/tutorials/filetex-samples/ex-csv-latex/chenbang_filecsv_latex.pdf)

* File TeX: [chenbang_filecsv_latex.tex](https://gitlab.com/thiminhnhut/latex/tree/master/tutorials/filetex-samples/ex-csv-latex/chenbang_filecsv_latex.tex)

* Các file TeX ví dụ: [examples](https://gitlab.com/thiminhnhut/latex/tree/master/tutorials/filetex-samples/ex-csv-latex/examples).
